# Mindaugas Sarskus

**Home address:** 27 Cois Chlair, Claregalway, co. Galway  
**Email:** minde.pinde@gmail.com  
**Mobile:** 0872 6996 52  
**More up to date CV:** [https://gitlab.com/My-/cv](https://gitlab.com/My-/cv)  
**Linkedin:** [https://www.linkedin.com/in/mindaugas-sarskus-09a189150/](https://www.linkedin.com/in/mindaugas-sarskus-09a189150/) 

[comment]: # (Add RQ code here pointing to my CV)
<img src="res/rq_code.png" width="100"/>

[comment]: # (Then a sentence about what you've achieved. And then a sentence about what you're looking for: what you would ideally be doing, with whom and in what environment.)

 **A final year student of the BSc (Hons) Software Development in Galway Mayo Institute of Technology, with a keen interest in math, programming, and problem solving. Skills: Java/Java8+, JS, Rust, C, Python, Git, JUnit, Maven, SpringBoot, CI/CD and others. Working experience in software testing, JS and Java. In addition I am a qualified carpenter/joiner where I have developed skills in meeting deadlines, teamwork and working under pressure. Looking forward to starting a career in software Development.**

## Industry Experience
#### SmartBear (Jun 2019 - September 2019)
**Role:** Software Intern
Responsibilities:

- Worked on front-end and back-end as well as in testing teams.
- Tasked with independently researching different search engines *Solr*, *Elasticsearch* alongside a fellow intern.
- Created a proof of concept (POC) to incorporate new search functionality using *Elasticsearch*.
- Copying data in the desired format from *MongoDB* to *Elasticsearch*.
- Used Reactive Java to create a custom fully asynchronous [MongoDB to Elasticsearch dumper](https://gitlab.com/My-/mongotoelastic).
-Full documentation of our POC along with a presentation of our work to the Back-end team.

- Attended all team stand-ups and company meetings.

## Education

#### 2016 to 2020 Galway-Mayo Institute of Technology
**Course:** BSc (Hons) in Software Development.  
**Key modules year 4:** Distributed Systems, Advance Object Oriented Programming, Applied Project and Minor Dissertation, Artificial Intelligence, Emerging Technologies.  
**Expected result:** 1.1     
**Key modules year 3:** Object Oriented Programming, Data Centric Web Applications, Professional Practice in IT, Software Testing, Database Management Systems.   
**Result:** 1.1 (82.00%)


## Projects:

- [Mongo to Elastic Dumper](https://gitlab.com/My-/mongotoelastic). (Java8+) - Created to help with the task we had during internship at SmartBear.
- [Blockchain based Messaging App](https://gitlab.com/My-/year3_project). (Rust) - Language and technologies I have never used before.
- [Thompson construction](https://gitlab.com/My-/year3_project). (Python) - Learned python while building this program. Took the opportunity to practice/implement [CI](https://gitlab.com/My-/thompson_construction/pipelines) on this project.
- [Cosine Distance](https://gitlab.com/My-/cosinedistance). (Java 8) Used Java features I learned on my own such as CompleatableFeatures, Thread pools, Java 8 Streams.
- [Recipe App](https://github.com/My-/CRUD-MEAN-app). (NodeJS, MongoDB, JWT). The development of the backed of the Web Application was the focal point of this project.
- [Baby Test](https://github.com/My-/C_BabyTest) (C) The simple testing library I created to help me with the C project I did at the time.

## Key Skills:

- #### Languages:

    - **Java/Java 8+:** Proficient understanding of Java Collections API. Comfortable using relatively new Java 8 features: Streams and Lambdas for my coding work. OOP is one of my strong points.
    - **JavaScript:** Learned JS during the summer of '17 by solving many of [CodeWars.com](https://www.codewars.com/users/My-/completed) coding challenges. Proficiency with JS ES6 features.
    - **Rust:** Picked Rust as the language for my main project in third year at GMIT to build a messaging app with Blockchain as the Backend. Rust is a system language that runs at similar speeds to C because it has no garbage collection. It uses a borrowing mechanism to manage memory, this makes it a complex language with slow compile times. By picking a new language like Rust to use for programming I was hoping to prove that I can adapt and learn a new language if necessary.
    - **C:** Began learning C before college by tinkering with Arduino. Learned the more advance features such as pointers and memory allocation while studying at GMIT.
    - **Python:** Initially taught myself how to use python so I could take part the Cisco Jam in 2017. In 2019 I used Python to create [Thomson construction](https://gitlab.com/My-/thompson_construction) algorithm for the Graph Theory module.  


- #### Other Skills

    - **Git:** Good understanding of the main git commands. I am applying it to most of my coding now.  My [gitlab](https://gitlab.com/My-?nav_source=navbar) and [github](https://github.com/My-) pages.
    - **Testing:** Writing JUnit test cases to cover the functionality in my main projects. Experience writing tests for other languages.
    - **Linux:** Using Linux as my main operating system since 2017. Using it for all my college work and day to day tasks unless the task specifically requires Windows.
    - **Docker:** Good understanding of Docker. Used Docker to create super light images for the [Blockchain in Rust](https://gitlab.com/My-/year3_project) project.



#### Other experience

- Creator of GMIT Computing Society(2016 - 2018).
- PASS leader GMIT (2017)
- Full driving license: A, B.
- Carpenter/Joiner (pre college work/summer work).

#### Referees:
- **Dr. John Healy  
Lecturer in Software Development.**  
Dept. of Computer Science & Applied Physics,  
School of Science and Computing,  
Galway-Mayo Institute of Technology,  
Dublin Road, Galway  
Email: *\<Available on request>*  
Phone: *\<Available on request>*
- **Michael Melody**  
Java Developer at SmartBear,  
My internship mentor,  
Email: *\<Available on request>*  
Phone: *\<Available on request>*
